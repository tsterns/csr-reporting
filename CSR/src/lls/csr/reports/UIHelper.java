package lls.csr.reports;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import com.languageline.es.util.ElasticsearchClientWrapper;

public class UIHelper {
	
	public static String inContactBaseURL="https://HOME-C31.INCONTACT.COM/ReportService/DataDownloadHandler.ashx?CDST=";
	public static String inContactBaseParms="&Format=CSV&AppendDate=False&IncludeHeaders=";

	public static ArrayList<String> filterList(ArrayList<String> csv, int column, String filter, boolean inclusive) {
		int row=0;
		String value;
		
		while (row < csv.size()) {
			value = csv.get(row).split(",")[column];
			if ((inclusive && value.equalsIgnoreCase(filter)) || (!inclusive && !value.equalsIgnoreCase(filter)))
				row++;
			else
				csv.remove(row);
		}
		
		return csv;
	}
	
	public static ArrayList<String> getICReport(String appKey, String dateFrom, String dateTo, boolean header) {
		ArrayList<String> csv = new ArrayList<String>();
		
		String reportURL;
		
		DateTime fd = new LocalDateTime(dateFrom + "T00:00:00").toDateTime(DateTimeZone.UTC);
		DateTime td = new LocalDateTime(dateTo   + "T23:59:59").toDateTime(DateTimeZone.UTC);
		
		String DF = fd.toString("M/d/yyy hh:mm:ss a");
		String DT = td.toString("M/d/yyy hh:mm:ss a");
		
		//DateFrom=2/5/2018+08:00:00+AM&DateTo=2/6/2018+07:59:00+AM

		try {
			reportURL = inContactBaseURL + appKey + "&DateFrom=" + URLEncoder.encode(DF,"UTF-8") + "&DateTo=" + URLEncoder.encode(DT,"UTF-8") + inContactBaseParms + header;
			URL url = new URL(reportURL);
			URLConnection uc = url.openConnection();

			InputStream in = uc.getInputStream();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			
			for (String line; (line = reader.readLine()) != null;) {
				//System.out.println(line);
				csv.add(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return csv;
	}
	
	public static ArrayList<String> icTeamList(boolean headers, String date) {
		return filterList(getICReport(
			"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KLah5i6cAPNIuNsEFmjmyG4NotFBXTtdd4aDITztUwkVudr22J4v6QfQZ6bjuZLLRQIZvs%2f2Hric3DrB63%2fbkf5Mns9mCKyKPNDLazBzHQb4FCjeAyqrjkfJ1tQZE9TV%2bxLvKKFiwRVYLBeC7ASlzAnzEMN6pE%2bTFIi",
			date, date, headers),1,"Admin", false);
	}
	
	public static ArrayList<String> icCampaignList(boolean headers, String date) {
		return getICReport(
			"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KJHnEv2A4HLCkxN0QTXJXTbw%2bXkOMOYeDk1gLKF1BAiHStIGc0KLgbo23MEYEHE7VzlsNWq%2bsLOkb1E2comAdHe0mFVgYMbDUFEbOlLTn19Ue55MhykcJwVc7VKyf7XmUyx1ru8%2bV4bOSnYGJlaOnxVHUCqQZzj3Yi89Q%3d%3d",
			date, date, headers);
	}

	public static ArrayList<String> icSkillList(boolean headers, String date) {
		return getICReport(
			"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KJqExXqpOfeHpcS%2fNQbXUQDVMkYSzSkjLtO9ll7RH6Vdkuu50Cg95E0KRVRl7CpsMqlgSuzA%2bAJxo6KlB7ke8cemQG%2fmb0Qh9oKoFt8iqoZbzT7EvRccm2EYievsK9VOCItsmSOkVXs%2bh4Ln6PezmWKDhQJUzbuT%2fmO3w%3d%3d",
			date, date, headers);
	}

	public static ArrayList<String> icAgentSkillList(boolean headers, String date) {
		return getICReport(
			"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KLAeVyJrBVVL1ZsgyWP0AP%2bLADUlRe3KjyaakbCjwU3M9LPB1%2bND454akBd2eZf4Hl5PtWkpVgXD%2fXMvsbnSgPvazGfMnSECwQuEtw1IH4d66iRV94u0NDhzQZHKWfsZE4a3wGLATeu3FEAS6Fb9ix15ogSP2uE6yc%3d",
			date, date, headers);
	}
	
	public static ArrayList<String> icAgentList(String team, boolean headers, String date) {
		ArrayList<String> csv = filterList(getICReport(
			"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KLEbcr8S9mcHODU8oaeC0dRR0QIK2NMCy0HlVBp8VmhluBDPjzRASN4cBulh6U6scz4h5Fh86hvf1d5EZhuJiMaVXMZwedVmX%2f8Jey7wTZNNbVDk9OETAE78tVyoNOhUnk4xop2sX2Mcb%2br3HtqB3uUz4yHnINH5%2ftO",
			date, date, headers), 2, team, true);
		return csv;
	}
	
	public static Map<Long, AgentSummary> icAgentSummaries(String startDate) {
		String[] row;
		long agentNo;
		Map<Long, AgentSummary> agentSummaries = new HashMap<Long, AgentSummary>();
		ArrayList<String> summaries = getICReport(
				"lNvH09ti8Avl%2bPdXM133Ddg8cLBuXFgBc%2b3mtZWJ0KIiTFoMyhb5Zun5dCTq5tbNdL97Q3ABhVqo%2bfwFpC8FFKtxGI2pkPeqcs1D%2flD%2f7m4ZJOTE72Q1SbUCO7qJOPPL6pX0A3oiTuhLJx3XRcz06U91g2NXp%2feswuZ58lWOeVpgSZQn9LRIGSewKd%2b7YwLHuZCmiS%2fplPyv",
				startDate, startDate, false);

		for (int r=0;r<summaries.size();r++) {
			row = summaries.get(r).split(",");
			agentNo = Long.parseLong(row[0]);
			agentSummaries.put(agentNo, new AgentSummary(agentNo, row[1], Integer.parseInt(row[12]),0));
		}
		
		return agentSummaries;
		
	}
	
	public static ArrayList<String> esIVRcustom(ElasticsearchClientWrapper esClient, char what) {
		ArrayList<String> list = new ArrayList<String>();
		DateTime today = new DateTime().minusWeeks(1).withDayOfWeek(1);
		String term = "";
		String key;
		
		switch (what) {
			case 'c' : 
				term="category.keyword";
				break;
			case 'i' : 
				term="subCategory.keyword";
				break;
			case 'p' : 
				term="product.keyword";
				break;
		}
		
		try {
			BoolQueryBuilder esQuery = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("@timestamp")
					.from(today.withTimeAtStartOfDay()).to(today.plusDays(1).minusMillis(1)));
				
			SearchSourceBuilder builder = new SearchSourceBuilder().query(esQuery).size(0)
				.aggregation(AggregationBuilders.terms("items").field(term).order(Order.term(true)));

			SearchResponse results = esClient.search(new SearchRequest("csr-custom.*").source(builder));
			
			Terms items = results.getAggregations().get("items");
			
			for (Terms.Bucket item : items.getBuckets()) {
				key = item.getKeyAsString();
				if (!key.equalsIgnoreCase("--none--")) list.add(key);
			}


		} catch (IOException e) {
			e.printStackTrace();
		}

		
		return list;
	}
	
	
	public static void main(final String[] args) throws IOException {
		//ArrayList<String> csv = getICReport(appKey, dateFrom, dateTo, false);
		//ArrayList<String> csv = icAgentSkillList(true, new DateTime("2018-01-08"));
		
		//Map<Long, AgentSummary> mySummaries = icAgentSummaries("2018-02-05");
		//System.out.println(mySummaries);
		
		ElasticsearchClientWrapper esClient = LLSConfig.getElasticsearchClient();
		ArrayList<String> r = esIVRcustom(esClient, 'c');
		System.out.println("Categories:" + r.toString());

		r = esIVRcustom(esClient, 'i');
		System.out.println("Issues:" + r.toString());

		r = esIVRcustom(esClient, 'p');
		System.out.println("Products:" + r.toString());

		//for (int r=0;r<csv.size();r++)
		//	System.out.println(csv.get(r));
	}

}
