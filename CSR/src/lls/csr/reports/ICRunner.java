package lls.csr.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.languageline.es.util.ElasticsearchClientWrapper;

import lls.csr.reports.CustomerServiceReports;
import lls.csr.reports.UIHelper;

/**
 * Servlet implementation class CSRServlet
 */
@WebServlet("/ICRunner")
public class ICRunner extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public CustomerServiceReports report;
	ElasticsearchClientWrapper esClient;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ICRunner() {
        super();
    }
    
    String formatColumn(String value, int type, String cls, String stl) {
    	String result="<td class='" + cls + "' style='padding-right:30px; " + stl + "'>";
    	
    	switch (type) {
        	case 0 : 
        		Integer i = Integer.valueOf(value);
        		if (i==0)
        			result += "-</td>";
       			else
       				result += String.format("%d</td>", i);
        		break;
        	case 2 :
            	Double d = Double.valueOf(value);
            	if (d==0)
            		result += "-</td>";
            	else
            		result += String.format("%.1f%%</td>", d*100.0);
            	break;
        	}
    		
    	return result;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		class skillVals {
			ArrayList<String> y1;
			ArrayList<String> y2;
			boolean empty;
			
			skillVals(String y1, String y2) {
				this.y1 = new ArrayList<String>();
				this.y2 = new ArrayList<String>();
				this.empty=false;
				addValues(y1, y2);
			}
			void addValues(String y1, String y2) {
				this.y1.add(y1);
				this.y2.add(y2);
				this.empty=(y1.startsWith("0") && y2.startsWith("0"));
			}
		}
        
		class agentVals {
			ArrayList<String[]> y;
			boolean empty;
			
			agentVals(String[] yData) {
				this.y = new ArrayList<String[]>();
				this.empty=false;
				addValues(yData);
			}
			void addValues(String[] yData) {
				this.y.add(yData);
			}
		}
        
		ArrayList<String> csv = new ArrayList<String>();
		String requestType = request.getParameter("requestType");
		String today=new DateTime().toString("yyyy-MM-dd");
		
		if (requestType.equalsIgnoreCase("list")) {
			String list = request.getParameter("listType");
			switch (list) {
			case "campaigns" :
				csv = UIHelper.icCampaignList(false, today);
				break;
			case "skills" :
				csv = UIHelper.icSkillList(false, today);
				break;
			case "teams" :
				csv = UIHelper.icTeamList(false,today);
				break;
			case "agents" :
				String teamNo = request.getParameter("teamNo");
				csv = UIHelper.icAgentList(teamNo, false, today);
				break;
			case "ivr-categories" :
				csv = UIHelper.esIVRcustom(esClient, 'c');
				break;
			case "ivr-issues" :
				csv = UIHelper.esIVRcustom(esClient, 'i');
				break;
			case "ivr-products" :
				csv = UIHelper.esIVRcustom(esClient, 'p');
				break;
			}

			for (int c=0;c<csv.size()-1;c++) {
				response.getWriter().append(csv.get(c)+"|");
			}
			
			response.getWriter().append(csv.get(csv.size()-1));

		} else {
			String reportType  = request.getParameter("reportType");

			String startDate     = request.getParameter("startDate");
			String endDate       = request.getParameter("endDate");
			String teamNo        = request.getParameter("teamNo");
			String skills        = request.getParameter("skills");
			String proficiencies = request.getParameter("proficiency");
			String agentNo       = request.getParameter("agentNo");
			String agentName     = request.getParameter("agentName");
			
			StringBuffer result = new StringBuffer();
			String period = (reportType.contains("daily")) ? "Date" : "Time";
			String table;
			
			String t = "text-right";
			String b = "border-left:2px solid LightGray";

			switch (reportType) {
				case "skill-daily"  :
				case "skill-hourly" :
					
					table = "skill-" + ((reportType.contains("daily")) ? "daily" : "hourly") + "-table";
					
					csv = report.teamReport(esClient, reportType.contains("daily"), startDate, endDate, teamNo, skills, proficiencies);
					
					result.append(
						"<table class='table table-striped' id='" + table + "'><thead>" +
						"<tr><th colspan=2> </th>" +
						"<th class='bg-info text-white text-center' style='border-left:2px solid LightGray' colspan=3>Proficiency 1</th>" +
						"<th bgcolor='#ebf6f9' class=' text-white text-center' style='border-left:2px solid LightGray' colspan=3>Proficiency 2</th>" +
						"<th class='bg-info text-white text-center' style='border-left:2px solid LightGray' colspan=3>Totals</th></tr>" +
						"<tr>" +
							"<th>" + period + "</th><th>Skill</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray'>busy</th><th class='text-right'>calls</th><th class='text-right'>ratio</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray'>busy</th><th class='text-right'>calls</th><th class='text-right'>ratio</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray'>busy</th><th class='text-right'>calls</th><th class='text-right'>occupancy</th>" +
						"</tr></thead><tbody>");

					if (csv.size() > 0) {
						
						String xData  = "";
						Map<String,skillVals> sValues = new HashMap<String,skillVals>();

						String yElem1;
						String yElem2;
						
						for (int r=0;r<csv.size();r++) {
							String row[] = csv.get(r).split(",");

							if (!xData.contains(row[0]))
								xData += row[0]+",";
							
							if (!row[1].equalsIgnoreCase("summary")) {
								for (int p = 1; p < 3; p++) {
									
									yElem1 = row[p*3+1];
									yElem2 = row[p*3];
									
									if (!yElem1.equals("0.0") && ! yElem2.equals("0")) {
										if (!sValues.containsKey(row[1] + "/" + p))
											sValues.put(row[1] + "/" + p, new skillVals(yElem1, yElem2));
										else
											sValues.get(row[1] + "/" + p).addValues(yElem1, yElem2);
									}
									
								}
							}

							result.append((row[1].equalsIgnoreCase("summary"))? "<tr class='info'>" : "<tr>");
							result.append("<td>" + row[0] + "</td>");
							result.append("<td>" + row[1] + "</td>");
							
							result.append(formatColumn(row[2], 0, t, b) + formatColumn(row[3], 0, t, "") + formatColumn(row[4], 2, t, ""));		// p1
							result.append(formatColumn(row[5], 0, t, b) + formatColumn(row[6], 0, t, "") + formatColumn(row[7], 2, t, ""));		// p2
							result.append(formatColumn(row[8], 0, t, b) + formatColumn(row[9], 0, t, "") + formatColumn(row[10],2, t, ""));		// p3
						}

						result.append("</tr></tbody></table>!");
					
						// sort chart key items
						Map<String, skillVals> chartData = new TreeMap<String, skillVals>(sValues);
						
						String yData1="";
						String yData2="";
						
						for(Map.Entry<String, skillVals> s:chartData.entrySet()) {
							if (!s.getValue().empty) {
								yData1 += "|" + s.getKey() + ":";
								for (String y1 : s.getValue().y1) {
									yData1 += y1 + ",";
								}
								yData1 = yData1.substring(0, yData1.length()-1);

								yData2 += "|" +  s.getKey() + ":";
								for (String y2 : s.getValue().y2) {
									yData2 += y2 + ",";
								}
								yData2 = yData2.substring(0, yData2.length()-1);
							} else {
								System.out.println("empty set");
							}
						}
						result.append(xData.substring(0, xData.length()-1) + yData1 + "!" + yData2);
					}

					break;
				
				case "agent-daily"  :
				case "agent-hourly" :
					
					table  = "agent-" + ((reportType.contains("daily")) ? "daily" : "hourly") + "-table";
					
					csv = CustomerServiceReports.agentReport(esClient, reportType.contains("daily"), startDate, endDate, teamNo, agentNo);
					
					result.append(
						"<table class='table table-striped' id='" + table + "'><thead>" +
						"<tr><th>" + agentNo + " - " + agentName + "</th>" +
						"<th class='bg-info text-white text-center' style='border-left:2px solid LightGray' colspan=4>General Inquiry</th>" +
						"<th bgcolor='#ebf6f9' class=' text-white text-center' style='border-left:2px solid LightGray' colspan=4>Account Assistance</th>" +
						"<th class='bg-info text-white text-center' style='border-left:2px solid LightGray' colspan=4>Agent Metrics</th></tr>" +
						"<tr>" +
							"<th>" + period + "</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray' class='text-right'>prf</th><th class='text-right'>busy</th><th class='text-right'>calls</th><th class='text-right'>ratio</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray' class='text-right'>prf</th><th class='text-right'>busy</th><th class='text-right'>calls</th><th class='text-right'>ratio</th>" +
							"<th class='text-right' style='border-left:2px solid LightGray'>refused</th><th class='text-right'>asa</th><th class='text-right'>occupancy</th>" +
						"</tr></thead><tbody>");

					if (csv.size() > 0) {
						
						String xData  = "";
						Map<String,agentVals> aValues = new HashMap<String,agentVals>();

						String key;
						
						for (int r=0;r<csv.size();r++) {
							String row[] = csv.get(r).split(",");

							if (!xData.contains(row[2]))
								xData += row[2]+",";
							
							if (!row[1].equalsIgnoreCase("summary")) {
								String[] name = row[1].split(" ");
								key = name[name.length-1];
								
								if (!aValues.containsKey(key))
									aValues.put(key, new agentVals(new String[] { row[6], row[10], row[13], row[11], row[12], row[5], row[9] } ));
								else
									aValues.get(key).addValues(new String[] { row[6], row[10], row[13], row[11], row[12], row[5], row[9] } );
							}

							result.append((row[1].equalsIgnoreCase("summary"))? "<tr class='info'>" : "<tr>");
							result.append("<td>" + row[2] + "</td>");

							result.append(formatColumn(row[3], 0, t, b) + formatColumn(row[4], 0, t, "") + formatColumn(row[5], 0, t, "") + formatColumn(row[6], 2, t, "")); // p1
							result.append(formatColumn(row[7], 0, t, b) + formatColumn(row[8], 0, t, "") + formatColumn(row[9], 0, t, "") + formatColumn(row[10],2, t, "")); // p2
							result.append(formatColumn(row[11],0, t, b) + formatColumn(row[12],0, t, "") + formatColumn(row[13],2, t, ""));									// totals		
						}

						result.append("</tr></tbody></table>!");
					
						// sort chart key items
						Map<String, agentVals> chartData = new TreeMap<String, agentVals>(aValues);
						
						String[] yData = { "", "", "", "", "", "", "" };
						String[] yName = { "General Inquiry Ratio", "Account Assist Ratio", "Overall Occupancy", "Refusals", "ASA", "General Inquiry Calls", "Account Assist Calls" };
						
						for(Map.Entry<String, agentVals> a:chartData.entrySet()) {
							if (!a.getValue().empty) {
								for (int n=0;n<yName.length;n++) {
									yData[n] += "|" + yName[n] + ":";
									for (String[] y : a.getValue().y) {
										yData[n] += y[n] + ",";
									}
									yData[n] = "!" + yData[n].substring(0, yData[n].length()-1);
								}
							} else {
								System.out.println("empty set");
							}
						}

						String allY = yData[0] + yData[1] + yData[2] + yData[3] + yData[4] + yData[5] + yData[6];
						//System.out.println(allY);
						result.append(xData.substring(0, xData.length()-1) + allY);
					}

					break;
				case "ivr-daily"  :
				case "ivr-hourly" :

					String category    = request.getParameter("category");
					String filterField = request.getParameter("filterField");
					String filterValue = request.getParameter("filterValue");

					csv = CustomerServiceReports.ivrReport(esClient, reportType.contains("daily"), startDate, endDate, category, filterField, filterValue);
					
					for (String row : csv) {
						result.append(row);
					}
					
					break;

			}
			
			try {
				PrintWriter out = response.getWriter();
				out.print(result.toString());
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	@Override
	public void init() throws ServletException {
		esClient = LLSConfig.getElasticsearchClient();
		report = new CustomerServiceReports();
	}

	public void destroy() {
		try {
			LLSConfig.getElasticsearchClient().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.destroy();
	}

}
