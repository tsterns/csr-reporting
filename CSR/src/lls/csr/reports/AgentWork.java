package lls.csr.reports;

import org.joda.time.DateTime;

public class AgentWork {
	DateTime time;
	long skill;
	int proficiency;
	long busy;
	long idle;
	long calls;
	int refusals;
	
	AgentWork(DateTime time, long skill, boolean working, long duration, int proficiency, long calls) {
		this.time = time;
		this.skill = skill;
		this.proficiency = proficiency;

		if (working) {
			this.busy  = duration;
			this.calls = calls;
		} else
			this.idle = duration;
	}
	
	void addTime(boolean working, double duration, double calls) {
		if (working) {
			this.busy  += duration;
			this.calls += calls;
		} else
			this.idle += duration;
	}

}
