package lls.csr.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.languageline.config.util.ConfigData;
import com.languageline.es.util.ElasticsearchClientWrapper;

public class LLSConfig {
	private static final Logger log = LoggerFactory.getLogger(LLSConfig.class);
	private static ElasticsearchClientWrapper client;
	private static ConfigData conf;
	
	public synchronized static ConfigData getConf() {
		if (conf == null) {
			try {
				conf = new ConfigData("/etc/csr/connector.conf");
				return conf;
			} catch (Exception e) {
				System.err.println("Could not load config file: /etc/csr/connector.conf: "+e);
				e.printStackTrace();
			}
		}
		return conf;
	}
	public synchronized static ElasticsearchClientWrapper getElasticsearchClient() {
		if (client != null) {
			return client;
		}
		
		try {
			log.info("Initializing connection to elasticsearch");
			client = com.languageline.es.util.ElasticsearchClient.getRestClient(getConf());
		} catch (Exception e) {
			log.error("Unable to get elasticsearch client: {}",e,e);
		}
		
		return client;

	}
	
}