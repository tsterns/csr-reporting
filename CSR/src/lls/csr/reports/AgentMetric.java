package lls.csr.reports;

import org.joda.time.DateTime;

public class AgentMetric {
	DateTime time;
	char type;
	long refusals;
	long asa;
	
	AgentMetric(DateTime time, char type, long count) {
		this.time = time;
		this.type = type;
		if (type=='R')
			this.refusals = count;
		else
			this.asa = count;
	}
}
