package lls.csr.reports;

import org.joda.time.DateTime;

public class SkillSummary {
	DateTime time;
	long skill;
	long[] busy;
	long[] calls;
	long idle;
	
	SkillSummary(DateTime time, long skill, int proficiency, long busy, long idle, long calls) {
		this.busy  = new long[5];
		this.calls = new long[5];
		
		this.time  = time;
		this.skill = skill;
		
		addTime(proficiency, busy, idle, calls);
	}
	
	void addTime(int p, long busy, long idle, long calls) {
		this.busy[p] += busy;
		this.idle += idle;
		this.calls[p] += calls;
	}
	
}
