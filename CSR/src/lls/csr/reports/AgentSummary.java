package lls.csr.reports;

public class AgentSummary {
	long agentNo;
	String agentName;
	int refused;
	int asa;
	AgentSummary(long agentNo, String agentName, int refused, int asa) {
		this.agentNo = agentNo;
		this.agentName = agentName;
		this.refused = refused;
		this.asa = asa;
	}
}
