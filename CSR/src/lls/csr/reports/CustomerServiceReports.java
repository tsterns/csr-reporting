package lls.csr.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.languageline.es.util.ElasticsearchClientWrapper;

public class CustomerServiceReports {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceReports.class);
	int debugLevel=5;

	public static Map<Long, String> skillList;
	public static Map<String, Integer> agentSkills;
	
	public void getSkills(String date){
		skillList = new HashMap<Long, String>();
		ArrayList<String> csv = UIHelper.icSkillList(false, date);
		
		for (int r=0;r<csv.size();r++) {
			String row[] = csv.get(r).split(",");
			skillList.put(Long.parseLong(row[0]), row[1]);
		}
	}
	
	public String skillNoToName(long skillNo) {
		String name = skillList.get(skillNo); 
		return name;
	}
	
	public static void dumpTeamResults(ArrayList<String> rows) {

		String header[] = new String[]{"date","skill","busy(1)","calls(1)","ratio(1)","busy(2)","calls(2)","ratio(2)","busy(3)","calls(3)","ratio(3)","busy(t)","calls(t)","occupancy(t)"};
		int width[] = {-12,-25,10,10,13,10,10,13,10,10,13,10,10,13};

		for (int h=0;h<header.length;h++)
			System.out.print(String.format("%" + width[h] +"s", header[h]));
		
		System.out.println();

		for (int r=0;r<rows.size();r++) {
			String[] row = rows.get(r).split(",");
			for (int c=0;c < row.length; c++)
				System.out.print(String.format("%" + width[c] +"s", row[c]));
			System.out.println();
		}
	}
	
	public static void dumpAgentResults(ArrayList<String> rows) {

		String header[] = new String[]{"id", "name", "time","p(1)", "busy(1)","calls(1)", "ratio(1)","p(2)","busy(2)","calls(2)", "ratio(2)","refused","asa","occupancy"};
		int width[] = {-8,-25,-5,5,8,8,9,5,8,8,9,8,4,10};

		for (int h=0;h<header.length;h++)
			System.out.print(String.format("%" + width[h] +"s", header[h]));
		
		System.out.println();

		for (int r=0;r<rows.size();r++) {
			String[] row = rows.get(r).split(",");
			for (int c=0;c < row.length; c++)
				System.out.print(String.format("%" + width[c] +"s", row[c]));
			System.out.println();
		}
	}
	
	public static String buildOrList(String values, String field) {
		String result = field + ":";
		String list[];
		
		if (values.equals("*"))
			result += "*";
		else if (!values.contains(","))
			result += "\"" + values + "\"";
		else {
			result += "(";
			list = values.split(",");
			for (int i=0;i<list.length;i++)
				result += "\"" + list[i] + "\" OR ";
			result = result.substring(0, result.length()-4) + ")";
		}
		
		return result;
	}
	
	private static void getRefusals(ElasticsearchClientWrapper esClient, Map<Long, Agent> agentMap, boolean daily, DateTime startDateTime, String agentFilter)  throws IOException {
		
		DateTime curTime=null;
		
		long curAgent;
		long refusalCount;
		Agent myAgent;
		
		DateHistogramInterval interval = (daily) ? DateHistogramInterval.days(1) : DateHistogramInterval.hours(1);

		BoolQueryBuilder esQuery = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("@timestamp")
			.from(startDateTime).to(startDateTime.plusDays(1).minusMillis(1)))
			.must(QueryBuilders.queryStringQuery("agentNo:" + agentFilter));
		
		SearchSourceBuilder builder = null;
		
		builder = new SearchSourceBuilder().query(esQuery).size(0)
			.aggregation(AggregationBuilders.dateHistogram("date").field("@timestamp")
				.dateHistogramInterval(interval).timeZone(DateTimeZone.forID("America/Los_Angeles")).minDocCount(1)
					.subAggregation(AggregationBuilders.terms("agents").field("agentNo")));

		SearchResponse periodResults = esClient.search(new SearchRequest("csr-refusals.*").source(builder));
		
		//System.out.println(builder.toString());

		log.info("{} [{}] day refusals", startDateTime.toString("yyyy-MM-dd"), periodResults.getHits().getTotalHits());

		Histogram dates = periodResults.getAggregations().get("date");

		for (Histogram.Bucket date : dates.getBuckets()) {
			curTime = new DateTime(date.getKeyAsString());
			Terms agents = date.getAggregations().get("agents");
			
			for (Terms.Bucket agent : agents.getBuckets()) {
				curAgent = agent.getKeyAsNumber().longValue();
				refusalCount = agent.getDocCount();
				
				if (agentMap.containsKey(curAgent)) {
					myAgent = agentMap.get(curAgent);
					myAgent.addRefusals(curTime, refusalCount);
				}
			}
		}
	}

	private static void getASA(ElasticsearchClientWrapper esClient, Map<Long, Agent> agentMap, boolean daily, DateTime startDateTime, String agentFilter)  throws IOException {
		
		DateTime curTime=null;
		
		long curAgent;
		Agent myAgent;
		
		DateHistogramInterval interval = (daily) ? DateHistogramInterval.days(1) : DateHistogramInterval.hours(1);
		
		BoolQueryBuilder esQuery = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("@timestamp")
			.from(startDateTime).to(startDateTime.plusDays(1).minusMillis(1)))
			.must(QueryBuilders.queryStringQuery("agentNo:" + agentFilter));
		
		SearchSourceBuilder builder = null;
		
		builder = new SearchSourceBuilder().query(esQuery).size(0)
			.aggregation(AggregationBuilders.dateHistogram("date").field("@timestamp")
				.dateHistogramInterval(interval).timeZone(DateTimeZone.forID("America/Los_Angeles")).minDocCount(1)
					.subAggregation(AggregationBuilders.terms("agents").field("agentNo")
					.subAggregation(AggregationBuilders.avg("asa").field("routingTime"))));
		
		SearchResponse periodResults = esClient.search(new SearchRequest("csr-contacts.*").source(builder));
		
		//System.out.println(builder.toString());

		log.info("{} [{}] day contacts", startDateTime.toString("yyyy-MM-dd"), periodResults.getHits().getTotalHits());

		Histogram dates = periodResults.getAggregations().get("date");

		for (Histogram.Bucket date : dates.getBuckets()) {
			curTime = new DateTime(date.getKeyAsString());
			Terms agents = date.getAggregations().get("agents");
			
			for (Terms.Bucket agent : agents.getBuckets()) {
				curAgent = agent.getKeyAsNumber().longValue();
				Avg asa = agent.getAggregations().get("asa");
				
				if (agentMap.containsKey(curAgent)) {
					myAgent = agentMap.get(curAgent);
					myAgent.addASA(curTime, Math.round(asa.getValue()));
				}
			}
		}
	}

	private static Map<Long, Agent> buildAgentStateList(ElasticsearchClientWrapper esClient, boolean daily, DateTime startDateTime, String teamNo, String skillFilter, String agentFilter, String proficiencyFilter) throws IOException {
		Map<Long, Agent> agentMap = new HashMap<Long, Agent>();
		
		DateTime curTime=null;
		
		long curSkill;
		long curAgent;
		
		int curProficiency;

		int workCode;
		boolean isWork;
		long duration;
		long callCount;
		
		DateHistogramInterval interval = (daily) ? DateHistogramInterval.days(1) : DateHistogramInterval.hours(1);
		
		BoolQueryBuilder esQuery = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("startDate")
			.from(startDateTime).to(startDateTime.plusDays(1).minusMillis(1)))
				.must(QueryBuilders.queryStringQuery("teamNo:" + teamNo + " AND skillNo:(" + skillFilter + " || 0) AND agentNo:" + agentFilter + " AND proficiency:(" + proficiencyFilter + " || 0)"));
		
		SearchSourceBuilder builder = null;
		
		builder = new SearchSourceBuilder().query(esQuery).size(0)
			.aggregation(AggregationBuilders.dateHistogram("date").field("startDate")
				.dateHistogramInterval(interval).timeZone(DateTimeZone.forID("America/Los_Angeles"))
					.subAggregation(AggregationBuilders.terms("agents").field("agentNo")
						.subAggregation(AggregationBuilders.terms("skills").field("skillNo")
							.subAggregation(AggregationBuilders.terms("proficiencies").field("proficiency")
								.subAggregation(AggregationBuilders.terms("work").field("working")
									.subAggregation(AggregationBuilders.sum("duration").field("duration")))))));

		SearchResponse periodResults = esClient.search(new SearchRequest("csr-states.*").source(builder));
		
		//System.out.println(builder.toString());

		log.info("{} [{}] day transactions", startDateTime.toString("yyyy-MM-dd"), periodResults.getHits().getTotalHits());
		
		if (periodResults.getHits().getTotalHits() > 0) {
			Histogram dates = periodResults.getAggregations().get("date");

			for (Histogram.Bucket date : dates.getBuckets()) {
				curTime = new DateTime(date.getKeyAsString());
				
				Terms agents = date.getAggregations().get("agents");
				
				for (Terms.Bucket agent : agents.getBuckets()) {

					curAgent = agent.getKeyAsNumber().longValue();
					Terms skills = agent.getAggregations().get("skills");

					for (Terms.Bucket skill : skills.getBuckets()) {

						curSkill = skill.getKeyAsNumber().longValue();
						Terms proficiencies = skill.getAggregations().get("proficiencies");
						
						for (Terms.Bucket proficiency : proficiencies.getBuckets()) {

							curProficiency = proficiency.getKeyAsNumber().intValue();
							Terms work = proficiency.getAggregations().get("work");

							for (Terms.Bucket working : work.getBuckets()) {

								workCode  = working.getKeyAsNumber().intValue();
								isWork    = (workCode != 0);
								callCount = (workCode==1) ? working.getDocCount() : 0;

								Sum skillDuration = working.getAggregations().get("duration");
								duration = Math.round(skillDuration.getValue() / 60.0);

								if (agentMap.containsKey(curAgent)) {
									agentMap.get(curAgent).addWork(curTime, curSkill, isWork, curProficiency, duration, callCount);
								} else {
									agentMap.put(curAgent, new Agent(curTime, curSkill, isWork, curProficiency, duration, callCount));
								}
							}
						}
					}
				}
			}
			
			getRefusals(esClient, agentMap, daily, startDateTime, agentFilter);
			getASA(esClient, agentMap, daily, startDateTime, agentFilter);
		}

		return agentMap;
	}
	
	public static ArrayList<String> ivrReport(ElasticsearchClientWrapper esClient, boolean daily, String start, String end, String category, String filterField, String filterValue) throws IOException {
		
		Map<String,long[]> ivr = new HashMap<String,long[]>();
		ArrayList<String> result = new ArrayList<String>();

		DateHistogramInterval interval = (daily) ? DateHistogramInterval.days(1) : DateHistogramInterval.hours(1);
		String dateFormat = (daily) ? "YYYY-MM-dd" : "HH:mm";
		
		DateTime startDate = new DateTime(start);
		DateTime endDate   = new DateTime(end).plusDays(1).withTimeAtStartOfDay().minusMillis(1);
		
		String key;
		String xAxis = "";
		int d = 0;
		int days = 0;
		
		String filter = buildOrList(filterValue, filterField);
	
		BoolQueryBuilder esQuery = QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("@timestamp")
			.from(startDate).to(endDate))
				.must(QueryBuilders.queryStringQuery("category:" + category + " AND " + filter));
		
		SearchSourceBuilder builder = new SearchSourceBuilder().query(esQuery).size(0)
			.aggregation(AggregationBuilders.dateHistogram("date").field("@timestamp").format(dateFormat)
				.dateHistogramInterval(interval).timeZone(DateTimeZone.forID("America/Los_Angeles"))
					.subAggregation(AggregationBuilders.terms("options").field(filterField + ".keyword"))
					.subAggregation(AggregationBuilders.terms("messages").field("messageTaken"))
					.subAggregation(AggregationBuilders.terms("transfers").field("emergencyTransfer"))
		);

		SearchResponse periodResults = esClient.search(new SearchRequest("csr-custom.*").source(builder));
		
		log.info("{} [{}] day ivr transaction", startDate.toString("yyyy-MM-dd"), periodResults.getHits().getTotalHits());
		
		if (periodResults.getHits().getTotalHits() > 0) {

			Histogram dates = periodResults.getAggregations().get("date");
			days = dates.getBuckets().size();

			for (Histogram.Bucket date : dates.getBuckets()) {
				xAxis += date.getKeyAsString() + ",";
				Terms options = date.getAggregations().get("options");

				for (Terms.Bucket option : options.getBuckets()) {
					key = "b|" + option.getKeyAsString();
					if (!ivr.containsKey(key))
						ivr.put(key, new long[days]);
					
					ivr.get(key)[d] = option.getDocCount();
				}
				
				Terms messages = date.getAggregations().get("messages");

				for (Terms.Bucket message : messages.getBuckets()) {
					key = message.getKeyAsString();
					if (key.equals("true")) {
						if (!ivr.containsKey("s|Messages Left"))
							ivr.put("s|Messages Left", new long[days]);
					
						ivr.get("s|Messages Left")[d] = message.getDocCount();
					}
				}

				Terms transfers = date.getAggregations().get("transfers");

				for (Terms.Bucket transfer : transfers.getBuckets()) {
					key = transfer.getKeyAsString();
					if (key.equals("true")) {
						if (!ivr.containsKey("s|Emergency Transfers"))
							ivr.put("s|Emergency Transfers", new long[days]);
					
						ivr.get("s|Emergency Transfers")[d] = transfer.getDocCount();
					}
				}

				d++;
			}

			result.add(xAxis.substring(0,xAxis.length()-1) + "!");
			
			Map<String, long[]> chartData = new TreeMap<String, long[]>(ivr);
			
			for(Map.Entry<String, long[]> i:chartData.entrySet()) {
				String v = "";
				for (int y=0;y<days-1; y++)
					v += i.getValue()[y] + ",";
				v += i.getValue()[days-1];
					
				result.add(i.getKey() + "|" + v + "!");
			}
		}
		
		return result;
	}

	static double occupancy(double busy, double idle) {
		double result;
		result = (busy+idle > 0) ?  busy*1.0/(busy+idle) : 0.0;
		return result;
	}
	
	static double ratio(double busy, double total) {
		double result;
		result = (total > 0) ?  busy*1.0/(total) : 0.0;
		return result;
	}
	
	public ArrayList<String> teamReport(ElasticsearchClientWrapper esClient, boolean daily, String startDate, String endDate, String teamNo, String skills, String proficiencies) throws IOException {
		
		Map<String, SkillSummary> skillSummaries = new HashMap<String, SkillSummary>();
		
		SkillSummary summary;
		String key;
		
		Agent agent;
		AgentWork work;

		ArrayList<String> rows = new ArrayList<String>();
		
		DateTime reportStartDate = new DateTime(startDate);
		DateTime reportEndDate   = new DateTime(endDate).plusDays(1).minusMillis(1);
		
		String timeFormat=(daily) ? "YYYY-MM-dd" : "HH:mm";
		
		int days = Days.daysBetween(reportStartDate, reportEndDate).getDays();
			
		for (int day = 0; day <= days; day++) {
				
			getSkills(reportStartDate.plusDays(day).toString("yyyy-MM-dd"));
				
			long[] totalBusy  = new long[10];
			long[] totalCalls = new long[10];
			long idleTotal = 0;

			Map<Long, Agent> agentList = buildAgentStateList(esClient, daily, reportStartDate.plusDays(day), teamNo, skills, "*", proficiencies);
			log.info("{} : {} agent summaries", reportStartDate.plusDays(day).toString(timeFormat), agentList.size());
				
			if (agentList.size() > 0) {
				for(Map.Entry<Long, Agent> a:agentList.entrySet()) {  
					
					agent = a.getValue();
						
					for (Map.Entry<String, AgentWork> aw:agent.work.entrySet()) {
							
						key  = aw.getKey();
						work = aw.getValue();
							
						if (!skillSummaries.containsKey(key)) 
							skillSummaries.put(key, new SkillSummary(work.time, work.skill, work.proficiency, work.busy, work.idle, work.calls));
						else {
							summary = skillSummaries.get(key);
							summary.addTime(work.proficiency, work.busy, work.idle, work.calls);
						}
						idleTotal += work.idle;
					}
						
				}
					
				String row="";
					
				for (Map.Entry<String, SkillSummary> s:skillSummaries.entrySet()) {
					key = s.getKey();
					summary = s.getValue();
						
					double skillBusy=0;
					double skillCalls=0;

					skillBusy     += summary.busy[1]  + summary.busy[2]; 
					skillCalls    += summary.calls[1] + summary.calls[2];

					totalBusy[1]  += summary.busy[1];
					totalCalls[1] += summary.calls[1];

					totalBusy[2]  += summary.busy[2];
					totalCalls[2] += summary.calls[2];
						
					if (summary.skill != 0 && summary.skill != 4061245) {
						row = String.format("%s,%s", summary.time.toString(timeFormat), skillNoToName(summary.skill));
						row = row + String.format(",%d,%d,%.31f", summary.busy[1], summary.calls[1], ratio(summary.busy[1],skillBusy));
						row = row + String.format(",%d,%d,%.31f", summary.busy[2], summary.calls[2], ratio(summary.busy[2],skillBusy));
						row = row + String.format(",%.0f,%.0f,%.31f", skillBusy, skillCalls, occupancy(skillBusy, idleTotal));
						rows.add(row);
					}
				}
					
				Collections.sort(rows);
					
				long summaryBusy  = totalBusy[1]  + totalBusy[2];  //+ totalBusy[3];
				long summaryCalls = totalCalls[1] + totalCalls[2]; //+ totalCalls[3];
				
				String summaryTime = (daily) ? reportStartDate.plusDays(day).toString(timeFormat) : "24:00";
					
				rows.add(String.format("%s,%s,%d,%d,%.3f,%d,%d,%.3f,%d,%d,%.3f",
					summaryTime, "Summary", 
					totalBusy[1], totalCalls[1], ratio(totalBusy[1], summaryBusy),
					totalBusy[2], totalCalls[2], ratio(totalBusy[2], summaryBusy),
					summaryBusy, summaryCalls, occupancy(summaryBusy, summaryCalls)
				));
					
				skillSummaries.clear();
			}
			
		}

		dumpTeamResults(rows);
		return rows;
	}
	
	public static ArrayList<String> agentReport(ElasticsearchClientWrapper esClient, boolean daily, String startDate, String endDate, String teamNo, String agent) throws IOException {
		
		Map<String, AgentStatesSummary> agentSummaries = new HashMap<String, AgentStatesSummary>();
		
		AgentStatesSummary summary;
		AgentSummary currentAgent;
		
		long curAgent;
		String agentName="";
		
		Agent agentObj;
		AgentWork work;
		AgentMetric metric;

		ArrayList<String> rows = new ArrayList<String>();
		String row;
		
		DateTime reportStartDate = new DateTime(startDate);
		DateTime reportEndDate   = new DateTime(endDate).plusDays(1).minusMillis(1);
		
		String timeFormat=(daily) ? "YYYY-MM-dd" : "HH:mm";
		String agentKey;
		
		String rowAgent;
		String rowTime;
		
		int days = Days.daysBetween(reportStartDate, reportEndDate).getDays();
			
		for (int day = 0; day <= days; day++) {
				
			// get agent summaries from inContact - {Agent Name, Refused Count, ASA}
			Map<Long, AgentSummary> agentList = UIHelper.icAgentSummaries(reportStartDate.plusDays(day).toString("YYYY-MM-dd"));
			
			Map<Long, Agent> agentStates= buildAgentStateList(esClient, daily, reportStartDate.plusDays(day), teamNo, "*", agent, "*");
			log.info("{} : {} agent summaries", reportStartDate.plusDays(day).toString(timeFormat), agentStates.size());
				
			if (agentStates.size() > 0) {
				for(Map.Entry<Long, Agent> a:agentStates.entrySet()) {  
					
					curAgent = a.getKey();
					agentObj = a.getValue();
					
					if (agentList.containsKey(curAgent)) {
						currentAgent = agentList.get(curAgent);
						agentName = currentAgent.agentName;
					}
					else
						System.out.println("oops!");

					for (Map.Entry<String, AgentWork> aw:agentObj.work.entrySet()) {
						
						work = aw.getValue();
						agentKey = curAgent+"|"+work.time.toString(timeFormat);

						if (!agentSummaries.containsKey(agentKey))
							agentSummaries.put(agentKey, new AgentStatesSummary());

						summary = agentSummaries.get(agentKey);
						summary.agentName = agentName;
						
						switch ((int)work.skill) {
						case 4049256 :
							summary.gen[0] = work.proficiency;
							summary.gen[1] = work.busy;
							summary.gen[2] = work.calls;
							summary.idle += work.idle;
							break;
						case 4049255 :
							summary.acc[0] = work.proficiency;
							summary.acc[1] = work.busy;
							summary.acc[2] = work.calls;
							summary.idle += work.idle;
							break;
						default :
							summary.idle += work.idle;
							break;
						}
						
					}
					
					for (Map.Entry<String, AgentMetric> am:agentObj.metrics.entrySet()) {

						metric = am.getValue();
						
						agentKey = curAgent+"|"+metric.time.toString(timeFormat);
						summary = agentSummaries.get(agentKey);
						
						if (metric.type=='R')
							summary.refused = metric.refusals;
						else
							summary.asa = metric.asa;
					}
						
				}
				
				// sort by agentNo, Name, Time
				Map<String, AgentStatesSummary> sorted = new TreeMap<String, AgentStatesSummary>(agentSummaries);
					
				for (Map.Entry<String, AgentStatesSummary> s:sorted.entrySet()) {
					rowAgent = s.getKey().split("\\|")[0];
					rowTime  = s.getKey().split("\\|")[1];
					summary  = s.getValue();
						
					row = String.format("%s,%s,%s", rowAgent, summary.agentName, rowTime);
					
					row = row + String.format(",%d,%d,%d,%.3f", summary.gen[0], summary.gen[1], summary.gen[2], ratio(summary.gen[1], summary.gen[1] + summary.acc[1]));
					row = row + String.format(",%d,%d,%d,%.3f", summary.acc[0], summary.acc[1], summary.acc[2], ratio(summary.acc[1], summary.gen[1] + summary.acc[1]));
					row = row + String.format(",%d,%d,%.3f",  summary.refused, summary.asa, occupancy(summary.gen[1] + summary.acc[1], summary.idle));
					
					rows.add(row);
				}
				
				agentSummaries.clear();
			}
			
		}

		dumpAgentResults(rows);
		return rows;
	}
	
	public static void main(String[] args) {
		CustomerServiceReports rpt = new CustomerServiceReports();
		try {
			ElasticsearchClientWrapper esClient = LLSConfig.getElasticsearchClient();
			rpt.teamReport(esClient, true, "2018-01-08", "2018-01-08", "405010", "*", "2");
			//rpt.agentReport(esClient, true, "2018-03-05", "2018-03-09", "405010", "6799270");
			//ArrayList<String> test = rpt.ivrReport(esClient, true, "2018-03-05", "2018-03-09", "Billing", "subCategory", "*");
			//System.out.println(test);
			
			LLSConfig.getElasticsearchClient().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
