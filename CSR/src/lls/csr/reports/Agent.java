package lls.csr.reports;

import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;

public class Agent {
	Map<String, AgentWork> work;
	Map<String, AgentMetric> metrics;
	int lastWorkProficiency;
	
	public Agent(DateTime time, long skillNo, boolean working, int proficiency, long duration, long calls) {
		this.work = new HashMap<String, AgentWork>();
		this.metrics = new HashMap<String, AgentMetric>();
		
		this.work.put(time.toString("HH")+":"+skillNo, new AgentWork(time, skillNo, working, duration, proficiency, calls));
	}
	
	public void addWork(DateTime time, long skillNo, boolean working, int proficiency, long duration, long calls) {
		AgentWork workItem;
		String key = time.toString("HH:") + skillNo;
		
		if (working && (proficiency==1 || proficiency==2))
			this.lastWorkProficiency=proficiency;
		
		if (proficiency==3) proficiency=this.lastWorkProficiency;
		
		if (this.work.containsKey(key)) {
			workItem = work.get(key);
			workItem.addTime(working, duration, calls);
		} else
			this.work.put(key, new AgentWork(time, skillNo, working, duration, proficiency, calls));
	}

	public void addRefusals(DateTime time, long count) {
		String key = time.toString("HH:") + "R";
		this.metrics.put(key, new AgentMetric(time, 'R', count));
	}
	
	public void addASA(DateTime time, long count) {
		String key = time.toString("HH:") + "A";
		this.metrics.put(key, new AgentMetric(time, 'A', count));
	}
}
