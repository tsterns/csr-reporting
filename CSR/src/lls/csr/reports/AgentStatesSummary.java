package lls.csr.reports;

public class AgentStatesSummary {
	String agentName;
	long[] gen;
	long[] acc;
	long idle;
	double occupancy;
	long refused;
	long asa;

	public AgentStatesSummary() {
		this.agentName = "";
		this.gen = new long[3];
		this.acc = new long[3];
		this.idle = 0;
		this.occupancy = 0;
		this.refused = 0;
		this.asa = 0;
	}
}
