/**
 * 
 */
var skill_startDate = moment();
var skill_endDate = moment();

var agent_startDate = moment();
var agent_endDate = moment();

var ivr_startDate = moment();
var ivr_endDate = moment();

var colors  = [ 'DodgerBlue', 'Orange', 'Green', 'Red', 'Purple', 'Gray', 'LightGray', 'LightGreen', 'Blue', 'Indigo', 'SlateGray', 'Navy', 'LightGreen', 'Blue', 'Indigo', 'SlateGray', 'Navy' ];
var symbols = [ 'circle', 'square', 'diamond', 'triangle-up', 'hexagon' ];
var yxs     = ['', '', 'y2', 'y2', '' ];

$(document).ready(function() {

  function errorMsg(message) {
    bootbox.alert({
      message: message,
      size:    'small',
      animate: true})
      .find('.modal-content').css({
        'margin-top': 
          function (){
            var w = $( window ).height()-$( window ).height()/4;
            var b = $(".modal-dialog").height();
            var h = (w-b)/2;
            return h+"px";
        }
      });
  }
  
  $('#spinner-label').hide();
  $('#spinner-wheel').hide();

  function showLoader(active) {
    if (active) {
      $('#spinner-label').show();
      $('#spinner-wheel').show();
    } else {
      $('#spinner-label').hide();
      $('#spinner-wheel').hide();
    }
  }

  $('input[name="skill-range"]').daterangepicker({
    ranges : {
      'Today' : [ moment(), moment() ],
      'Yesterday' : [ moment().subtract(1, 'days'), moment().subtract(1, 'days') ],
      'This Week' : [ moment().startOf('week'), moment().endOf('week') ],
      'Last Week' : [ moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week') ],
      'This Month' : [ moment().startOf('month'), moment().endOf('month') ],
      'Last Month' : [ moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month') ]
    },
    locale : {
      format : 'YYYY-MM-DD',
      cancelLabel : 'Clear'
    },
    autoApply: true,
    startDate: moment(),
    endDate:   moment()
    }, function(start, end) {
      $('#skill-range span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
      skill_startDate = start;
      skill_endDate = end;
      $('#skill-date').datepicker('setDate', skill_startDate.toDate());
    } 
  );

  $('input[name="agent-range"]').daterangepicker({
    ranges : {
      'Today' : [ moment(), moment() ],
      'Yesterday' : [ moment().subtract(1, 'days'), moment().subtract(1, 'days') ],
      'This Week' : [ moment().startOf('week'), moment().endOf('week') ],
      'Last Week' : [ moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week') ],
      'This Month' : [ moment().startOf('month'), moment().endOf('month') ],
      'Last Month' : [ moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month') ]
    },
    locale : {
      format : 'YYYY-MM-DD',
      cancelLabel : 'Clear'
    },
    autoApply: true,
    startDate: moment(),
    endDate:   moment()
    }, function(start, end) {
      $('#agent-range span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
      agent_startDate = start;
      agent_endDate = end;
      $('#agent-date').datepicker('setDate', agent_startDate.toDate());
    }
  );

  $('input[name="ivr-range"]').daterangepicker({
    ranges : {
      'Today' : [ moment(), moment() ],
      'Yesterday' : [ moment().subtract(1, 'days'), moment().subtract(1, 'days') ],
      'This Week' : [ moment().startOf('week'), moment().endOf('week') ],
      'Last Week' : [ moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week') ],
      'This Month' : [ moment().startOf('month'), moment().endOf('month') ],
      'Last Month' : [ moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month') ]
    },
    locale : {
      format : 'YYYY-MM-DD',
      cancelLabel : 'Clear'
    },
    autoApply: true,
    startDate: moment(),
    endDate:   moment()
    }, function(start, end) {
      $('#ivr-range span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
      ivr_startDate = start;
      ivr_endDate = end;
      $('#ivr-date').datepicker('setDate', ivr_startDate.toDate());
    }
  );

  Date.prototype.yyyymmdd = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();

    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
  };

  $('#skill-date').datepicker({
    autoclose:          true,
    format:             'yyyy-mm-dd',
    immediateUpdates:   true,
    startDate:          '-6m',
    todayBtn:           'linked',
    todayHighlight :    true
  });

  $('#skill-date').datepicker('setDate', new Date());

  $('#skill-single-date').hide();
  $('#skill-run-hourly').hide();

  $('#agent-date').datepicker({
    autoclose:          true,
    format:             'yyyy-mm-dd',
    immediateUpdates:   true,
    startDate:          '-6m',
    todayBtn:           'linked',
    todayHighlight :    true
  });

  $('#agent-date').datepicker('setDate', new Date());

  $('#agent-single-date').hide();
  $('#agent-run-hourly').hide();

  $('#ivr-date').datepicker({
    autoclose:          true,
    format:             'yyyy-mm-dd',
    immediateUpdates:   true,
    startDate:          '-6m',
    todayBtn:           'linked',
    todayHighlight :    true
  });

  $('#ivr-date').datepicker('setDate', new Date());

  $('#ivr-single-date').hide();
  $('#ivr-run-hourly').hide();

  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
    var target = $(e.target).attr("href") // activated tab
    switch (target) {
      case "#skill-daily-tab":
        $('#skill-single-date').hide();
        $('#skill-run-hourly').hide();
        $('#skill-date-range').show();
        $('#skill-run-daily').show();
        break;
      case "#skill-hourly-tab":
        $('#skill-date-range').hide();
        $('#skill-run-daily').hide();
        $('#skill-single-date').show();
        $('#skill-run-hourly').show();
        break;
      case "#agent-daily-tab":
        $('#agent-single-date').hide();
        $('#agent-run-hourly').hide();
        $('#agent-date-range').show();
        $('#agent-run-daily').show();
        break;
      case "#agent-hourly-tab":
        $('#agent-date-range').hide();
        $('#agent-run-daily').hide();
        $('#agent-single-date').show();
        $('#agent-run-hourly').show();
        break;
      case "#ivr-daily-tab":
        $('#ivr-single-date').hide();
        $('#ivr-run-hourly').hide();
        $('#ivr-date-range').show();
        $('#ivr-run-daily').show();
        break;
      case "#ivr-hourly-tab":
        $('#ivr-date-range').hide();
        $('#ivr-run-daily').hide();
        $('#ivr-single-date').show();
        $('#ivr-run-hourly').show();
        break;
    }
  });

  loadTeams($('#skill-team-picker'));
  loadTeams($('#agent-team-picker'));

  function loadTeams(select) {
    var teams = $.post('ICRunner', {
      requestType : "list",
      listType : 'teams'
    });

    teams.done(function(data) {
      var opts = data.split("|");
      select.empty();

      for (i = 0; i < opts.length; i++) {
        if (opts[i].length > 0) {
          var vals = opts[i].split(",");
          select.append($("<option></option>").attr("value", vals[0]).text(vals[1]));
        }
      }
      select.selectpicker('refresh');
      select.trigger('change');
    });
  }

  // load Skills
  var skills = $.post('ICRunner', {
    requestType : "list",
    listType : 'skills'
  });

  skills.done(function(data) {
    var opts = data.split("|");
    var select = $('#skill-skill-picker');

    select.empty();
    select.append($("<option></option>").attr("value", "*").text("All Skills"));

    for (i = 0; i < opts.length; i++) {
      if (opts[i].length > 0) {
        var vals = opts[i].split(",");
        select.append($("<option></option>").attr("value", vals[0]).text(vals[1]));
      }
    }
    select.selectpicker('refresh');
  });

  $("#agent-team-picker").change(function() {
    console.log("entering agent-team-picker change:[" + $(this).val() + "]");

    // load Agents
    var agents = $.post('ICRunner', {
      requestType : "list",
      listType : 'agents',
      teamNo : $(this).val()
    });

    agents.done(function(data) {
      var opts = data.split("|");
      var select = $('#agent-picker-single');

      select.empty();

      for (i = 0; i < opts.length; i++) {
        if (opts[i].length > 0) {
          var vals = opts[i].split(",");
          select.append($("<option></option>").attr("value", vals[0]).text(vals[1]));
        }
      }
      select.selectpicker('refresh');
    });
  });

  // load IVR Categories
  var list = $.post('ICRunner', {
    requestType : "list",
    listType : 'ivr-categories'
  });

  list.done(function(data) {
    var opts = data.split("|");
    var select = $('#ivr-category-picker');

    select.empty();

    for (i = 0; i < opts.length; i++) {
      if (opts[i].length > 0) {
        select.append($("<option></option>").attr("value", opts[i]).text(opts[i]));
      }
    }
    //select.selectpicker('val', opts[0]);
    select.selectpicker('refresh');
  });

  $("#ivr-category-picker").change(function() {
    console.log("entering ivr-category-picker change:[" + $(this).val() + "]");

    var category = $('#ivr-category-picker').val().toString();
    var listType = '';
    
    if (category == 'Billing') {
      // load Issues
      listType = 'ivr-issues';
      $('#subcat-label').text("Issue");
    } else if (category == 'Customer Service') {
      listType = 'ivr-products';
      $('#subcat-label').text("Product");
    } else {
      $('#ivr-subcat').hide();
      return;
    }
    
    if (listType > '') {
      var list = $.post('ICRunner', {
        requestType: "list",
        listType:    listType,
      });
      $('#ivr-subcat').show();
    }

    list.done(function(data) {
      var opts = data.split("|");
      var select = $('#ivr-subcat-picker');

      select.empty();
      //select.append($("<option></option>").attr("value", '*').text('All'));

      for (i = 0; i < opts.length; i++) {
        if (opts[i].length > 0) {
          var vals = opts[i].split(",");
          select.append($("<option></option>").attr("value", vals[0]).text(vals[0]));
        }
      }
      //select.selectpicker('val', opts[0]);
      select.selectpicker('refresh');
    });
  });


  $('#skill-run-daily').click(function(event) {

    showLoader(true);
    $('#skill-run-daily').button('loading');

    startDate = skill_startDate.format('YYYY-MM-DD');
    stopDate = skill_endDate.format('YYYY-MM-DD');
    teamNo = $('#skill-team-picker').val().toString();
    skills = $('#skill-skill-picker').val().toString();
    proficiency = $('#skill-proficiency-picker').val().toString();

    console.log(startDate, stopDate, teamNo, skills, proficiency);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "skill-daily",
      startDate   : startDate,
      endDate     : stopDate,
      teamNo      : teamNo,
      skills      : skills,
      agents      : "*",
      proficiency : proficiency
    });

    // save the results
    occupancyData.done(function(data) {
      drawDailySkillResults(data);
      $('#skill-run-daily').button('reset');
    });

  });

  $('#skill-run-hourly').click(function(event) {

    showLoader(true);
    $('#skill-run-hourly').button('loading');

    start = $("#skill-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    stop = $("#skill-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    teamNo = $('#skill-team-picker').val().toString();
    skills = $('#skill-skill-picker').val().toString();
    proficiency = $('#skill-proficiency-picker').val().toString();

    console.log(start, stop, teamNo, skills, proficiency);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "skill-hourly",
      startDate   : start,
      endDate     : stop,
      teamNo      : teamNo,
      skills      : skills,
      agents      : "*",
      proficiency : proficiency
    });

    // save the results
    occupancyData.done(function(data) {
      drawHourlySkillResults(data);
      $('#skill-run-hourly').button('reset');
    });

  });

  $('#agent-run-daily').click(function(event) {

    startDate = agent_startDate.format('YYYY-MM-DD');
    stopDate  = agent_endDate.format('YYYY-MM-DD');
    teamNo    = $('#agent-team-picker').val().toString();
    agentNo   = $('#agent-picker-single').val().toString();
    agentName = $('#agent-picker-single option:selected').text();

    if (!agentNo) {
      errorMsg('Please select an agent for your report.');
      return
    }
    
    showLoader(true);
    $('#agent-run-daily').button('loading');

    console.log(startDate, stopDate, teamNo, agents);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "agent-daily",
      startDate   : startDate,
      endDate     : stopDate,
      teamNo      : teamNo,
      agentNo     : agentNo,
      agentName   : agentName
    });

    // save the results
    occupancyData.done(function(data) {
      drawDailyAgentResults(data, agentName);
      $('#agent-run-daily').button('reset');
    });

  });

  $('#agent-run-hourly').click(function(event) {

    start     = $("#agent-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    stop      = $("#agent-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    teamNo    = $('#agent-team-picker').val().toString();
    agentNo   = $('#agent-picker-single').val().toString();
    agentName = $('#agent-picker-single option:selected').text();

    if (!agentNo) {
      errorMsg('Please select an agent for your report.');
      return
    }
    
    showLoader(true);
    $('#agent-run-hourly').button('loading');

    console.log('running agent hourly: ',start, stop, teamNo, agentNo, agentName);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "agent-hourly",
      startDate   : start,
      endDate     : stop,
      teamNo      : teamNo,
      agentNo     : agentNo,
      agentName   : agentName
    });

    // save the results
    occupancyData.done(function(data) {
      drawHourlyAgentResults(data, agentName);
      $('#agent-run-hourly').button('reset');
    });

  });

  $('#ivr-run-daily').click(function(event) {

    startDate   = ivr_startDate.format('YYYY-MM-DD');
    stopDate    = ivr_endDate.format('YYYY-MM-DD');
    category    = $('#ivr-category-picker option:selected').text();
    filterValue = $('#ivr-subcat-picker').val().toString();

    if (category == 'Customer Service') {
      filterField = 'product';
    } else {
      filterField = 'subCategory';
    }
    
    if (filterValue.length==0)
      filterValue = '*';
    
    showLoader(true);
    $('#ivr-run-daily').button('loading');

    console.log(startDate, stopDate, filterField, filterValue);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "ivr-daily",
      startDate   : startDate,
      endDate     : stopDate,
      category    : category,
      filterField : filterField,
      filterValue : filterValue
    });

    // save the results
    occupancyData.done(function(data) {
      drawIVRResults(data, true);
      $('#ivr-run-daily').button('reset');
    });

  });

  $('#ivr-run-hourly').click(function(event) {

    startStop = $("#ivr-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    category    = $('#ivr-category-picker option:selected').text();
    filterValue = $('#ivr-subcat-picker option:selected').val();

    if (category == 'Customer Service') {
      filterField = 'product';
    } else {
      filterField = 'subCategory';
    }

    showLoader(true);
    $('#ivr-run-hourly').button('loading');

    console.log(startStop, filterField, filterValue);

    var occupancyData = $.post('ICRunner', {
      requestType : "report",
      reportType  : "ivr-hourly",
      startDate   : startStop,
      endDate     : startStop,
      category    : category,
      filterField : filterField,
      filterValue : filterValue    });

    // save the results
    occupancyData.done(function(data) {
      drawIVRResults(data, false);
      $('#ivr-run-hourly').button('reset');
    });

  });

  function drawDailySkillResults(data) {
    raw = data.split("!");

    if (raw.length < 2) {

      errorMsg('No Results Found - check filters');
      showLoader(false);
      return;
    }

    $('#skill-daily-report').empty().append(raw[0]);
    lTable = $('#skill-daily-report #skill-daily-table').html();

    var table = $('#skill-daily-table').DataTable({
      "dom" : "<'row'<'col-md-8'B><'col-md-2'l><'col-sm-2'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
      buttons : [ 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print' ],
      'pagelength' : 5,
      'lengthMenu' : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ]
    });

    chart = document.getElementById('skill-daily-chart');

    var chartTitle = "Skill Utilzation - by Day";

    var d;
    var chartData = [];
    var utilData = raw[1].split("|");
    var callData = raw[2].split("|");

    var xData = utilData[0].split(',')

    for (d = 1; d < utilData.length; d++) {
      parts = utilData[d].split(":");
      chartData.push({
        x:       xData,
        name:    parts[0],
        y:       parts[1].split(','),
        type:    'scatter',
        mode:    'lines+markers',
        marker:  { color: colors[d-1], size: 7 },
        line:    { color: colors[d-1], shape: 'spline' },
        hoverlabel: { namelength: -1 }
      });
    }

    for (d = 1; d < callData.length; d++) {
      parts = callData[d].split(":");
      chartData.push({
        x : xData,
        y : parts[1].split(','),
        name : parts[0],
        yaxis : 'y2',
        type : 'bar',
        opacity : .4,
        marker : { color : colors[d - 1] },
        hoverlabel: { namelength: -1 },
      });
    }

    var layout = {
        title : chartTitle,
        showlegend : true,
        xaxis : {
          type : 'date',
          tickmode : 'linear',
          tick0 : xData[1],
          dtick : 86400000.0,
          tickformat : "%m-%d"
        },
        yaxis : {
          title : 'Utilization',
          zeroline : false,
          tickfont: { size: 8 },
          tickformat: '.1%',
        },
        yaxis2 : {
          title : 'Busy Minutes',
          overlaying : 'y',
          side : 'right',
          zeroline : false,
          gridcolor : '#b3e7ff',
          color : '#008bcc',
        },
        legend : {
          orientation : "v",
          x : 1.1,
          xanchor : 'left',
          y : 1,
          font : {
            size : 9
          },
          borderwidth : 1,
          bordercolor : '#cccccc',
          bgcolor : '#F5F5F5'
        }
    };
    
    Plotly.newPlot(chart, chartData, layout);
    showLoader(false);
  };

  function drawDailyAgentResults(data, agentName) {
    raw = data.split("!");

    if (raw.length < 2) {
      errorMsg('No Results Found - check filters');
      showLoader(false);
      return;
    }

    $('#agent-daily-report').empty().append(raw[0]);
    lTable = $('#agent-daily-report #agent-daily-table').html();

    var table = $('#agent-daily-table').DataTable({
      "dom" : "<'row'<'col-md-8'B><'col-md-2'l><'col-sm-2'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
      buttons : [ 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print' ],
      'pagelength' : 5,
      'lengthMenu' : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ]
    });
    
    chart = document.getElementById('agent-daily-chart');

    var chartTitle = agentName + " - Agent Utilzation - by Day";
    var chartData = [];
    var xData = raw[1].split(",");
    
    var c;
    var d;
    
    for (c = 2; c<raw.length; c++) {
      
      var yData = raw[c].split("|");
      
      for (d = 1; d < yData.length; d++) {
        elements = yData[d].split(":");
        if (c<=4) {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'scatter',
            mode:       'lines+markers',
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)], size: 8, symbol: symbols[c-2] },
            line:       { color: colors[(c-2)+(d-1)], shape: 'spline' }
          });
        } else if (c<=6) {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'scatter',
            mode:       'lines+markers',
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)], size: 8, symbol: symbols[c-2] },
            line:       { color: colors[(c-2)+(d-1)], shape: 'spline' },
            yaxis:      'y2'
          });
        } else {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'bar',
            opacity:    0.5,
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)] },
            yaxis:      'y2'
          });
        }
      }
    }
    
    var layout = {
      title : chartTitle,
      showlegend : true,
      barmode: 'stack',
      bargap: 0.80,
      xaxis : {
        type : 'date',
        tickmode : 'linear',
        tick0 : xData[1],
        dtick : 86400000.0,
        tickformat : "%m-%d"
      },
      yaxis : {
        title:    'Utilization/Occupancy',
        tickfont: { size: 8 },
        tickformat: '.1%',
        zeroline: false
      },
      yaxis2 : {
        title:      'Refusals/ASA/Mniutes',
        gridcolor:  '#e6e6ff',
        color:      'Navy',
        anchor:     'x',
        overlaying: 'y',
        side:       'right',
        zeroline:   false
      },
      legend : {
        orientation: 'v',
        x:           1.1,
        xanchor:    'left',
        y:           1,
        font:        { size: 9 },
        borderwidth: 1,
        bordercolor: '#cccccc',
        bgcolor:     '#F5F5F5',
        traceorder:  'normal'
      }
    };

    Plotly.newPlot(chart, chartData, layout);
    showLoader(false);

   };

   function drawIVRResults(data, daily) {
     raw = data.split("!");

     if (raw.length < 2) {
       errorMsg('No Results Found - check filters');
       showLoader(false);
       return;
     }

     chart = (daily) ? document.getElementById('ivr-daily-chart') : document.getElementById('ivr-hourly-chart');

     var chartTitle = (daily) ? 'IVR Customer Path - by Day' : 'IVR Customer Path - by Hour';
     var chartData = [];
     var xAxis = raw[0].split(',');
     
     var c;
     var d;
     
     for (c = 1; c<raw.length; c++) {
       
       if (raw[c].length>0) {
         var cData = raw[c].split("|");
         var isBar = (cData[0]=='b');
         chartData.push({
           name:        cData[1],
           x:           xAxis,
           y:           cData[2].split(','),
           namelength:  -1,
           type:        (isBar) ? 'bar' : 'scatter',
           opacity:     (isBar) ? 0.6 : 1.0,
           hoverlabel:  { namelength: -1 },
           marker:      { color: colors[(c-2)+(d-1)] },
           yaxis:       (isBar) ? 'y1' : 'y2'
         });
       }
     }
     
     tickType = (daily) ? 'date'  : 'time';
     tickFrmt = (daily) ? '%m-%d' : '%H:%M'
     
     var layout = {
       title : chartTitle,
       showlegend : true,
       xaxis : {
         type : tickType,
         tickmode : 'linear',
         tick0 : xAxis[0],
         tickformat : tickFrmt
       },
       yaxis : {
         title:    'Option Count',
         tickfont: { size: 8 },
         zeroline: false
       },
       yaxis2 : {
         title:      'Messages/Emergency Transfers',
         gridcolor:  '#e6e6ff',
         color:      'Navy',
         anchor:     'x',
         overlaying: 'y',
         side:       'right',
         zeroline:   false
       },
       legend : {
         orientation: 'v',
         x:           1.1,
         xanchor:    'left',
         y:           1,
         font:        { size: 9 },
         borderwidth: 1,
         bordercolor: '#cccccc',
         bgcolor:     '#F5F5F5',
         traceorder:  'normal'
       }
     };

     Plotly.newPlot(chart, chartData, layout);
     showLoader(false);

    };

  function drawHourlySkillResults(data) {
    raw = data.split("!");

    if (raw.length < 1) {
      errorMsg('No Results Found - check filters');
      showLoader(false);
      return;
    }

    $('#skill-hourly-report').empty().append(raw[0]);
    lTable = $('#skill-hourly-report #skill-hourly-table').html();

    var table = $('#skill-hourly-table').DataTable({
      "dom" : "<'row'<'col-md-8'B><'col-md-2'l><'col-sm-2'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
      buttons : [ 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print' ],
      'pagelength' : 5,
      'lengthMenu' : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ]
    });
    
    chart = document.getElementById('skill-hourly-chart');

    var chartTitle = "Skill Utilzation - by Hour";

    var d;
    var chartData = [];
    var utilData = raw[1].split("|");
    var callData = raw[2].split("|");

    var xData = utilData[0].split(',')

    for (d = 1; d < utilData.length; d++) {
      parts = utilData[d].split(":");
      chartData.push({
        x : xData,
        y : parts[1].split(','),
        name : parts[0],
        type : 'scatter',
        mode:    'lines+markers',
        marker:  { color: colors[d-1], size: 7 },
        line:    { color: colors[d-1], shape: 'spline' }
      });
    }

    for (d = 1; d < callData.length; d++) {
      parts = callData[d].split(":");
      chartData.push({
        x : xData,
        y : parts[1].split(','),
        name : parts[0],
        yaxis : 'y2',
        type : 'bar',
        opacity : .4,
        marker : { color : colors[d - 1] }
     });
    }

    var layout = {
      title : chartTitle,
      showlegend : true,
      xaxis : {
        type : 'time',
        tickmode : 'array',
        tickvals : xData,
        ticktext : xData
      },
      yaxis : {
        title : 'Utilization',
        zeroline : false,
        tickfont: { size: 8 },
        tickformat: '.1%',
      },
      yaxis2 : {
        title : 'Busy Minutes',
        overlaying : 'y',
        side : 'right',
        zeroline : false,
        gridcolor : '#b3e7ff',
        color : '#008bcc'
      },
      legend : {
        orientation : "v",
        x : 1.1,
        xanchor : 'left',
        y : 1,
        font : { size : 9 },
        borderwidth : 1,
        bordercolor : '#cccccc',
        bgcolor : '#F5F5F5'
      }
    };
    Plotly.newPlot(chart, chartData, layout);
    showLoader(false);
  };

  function drawHourlyAgentResults(data, agentName) {
    raw = data.split("!");

    if (raw.length < 2) {
      errorMsg('No Results Found - check filters');
      showLoader(false);
      return;
    }

    $('#agent-hourly-report').empty().append(raw[0]);
    lTable = $('#agent-hourly-report ' + '#agent-hourly-table').html();

    var table = $('#agent-hourly-table').DataTable({
      "dom" : "<'row'<'col-md-8'B><'col-md-2'l><'col-sm-2'f>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
      buttons : [ 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print' ],
      'pagelength' : 5,
      'lengthMenu' : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ]
    });
    
    chart = document.getElementById('agent-hourly-chart');

    var chartTitle = agentName + " - Agent Utilzation - by Hour";
    var chartData = [];
    var xData = raw[1].split(",");
    
    var c;
    var d;
    
    for (c = 2; c<raw.length; c++) {
      
      var yData = raw[c].split("|");
      
      for (d = 1; d < yData.length; d++) {
        elements = yData[d].split(":");
        if (c<=4) {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'scatter',
            mode:       'lines+markers',
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)], size: 8, symbol: symbols[c-2] },
            line:       { color: colors[(c-2)+(d-1)], shape: 'spline' }
          });
        } else if (c<=6) {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'scatter',
            mode:       'lines+markers',
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)], size: 8, symbol: symbols[c-2] },
            line:       { color: colors[(c-2)+(d-1)], shape: 'spline' },
            yaxis:      'y2'
          });
        } else {
          chartData.push({
            x:          xData,
            name:       elements[0],
            y:          elements[1].split(','),
            namelength: -1,
            type:       'bar',
            opacity:    0.8,
            hoverlabel: { namelength: -1 },
            marker:     { color: colors[(c-2)+(d-1)] },
            yaxis:      'y2'
          });
        }
      }
    }
    
    var layout = {
      title : chartTitle,
      showlegend : true,
      barmode: 'stack',
      bargap: 0.80,
      xaxis : {
        type:     'time',
        tickmode: 'array',
        tickvals: xData,
        ticktext: xData
      },
      yaxis : {
        title:    'Utilization/Occupancy',
        tickfont: { size: 8 },
        tickformat: '.1%',
        zeroline: false
      },
      yaxis2 : {
        title:      'Refusals/ASA/Minutes',
        side:       'right',
        gridcolor:  '#ccccff',
        color:      'Navy',
        overlaying: 'y',
        zeroline:   false
      },
      legend : {
        orientation: 'v',
        x:           1.1,
        xanchor:    'left',
        y:           1,
        font:        { size: 9 },
        borderwidth: 1,
        bordercolor: '#cccccc',
        bgcolor:     '#F5F5F5',
        traceorder:  'normal'
      }
    };

    Plotly.newPlot(chart, chartData, layout);
    
    showLoader(false);

  };

  function toggleSelectAll(control) {
    var allOptionIsSelected = (control.val() || []).indexOf("*") > -1;
    function valuesOf(elements) {
      return $.map(elements, function(element) {
        return element.value;
      });
    }

    if (control.data('allOptionIsSelected') != allOptionIsSelected) {
      if (allOptionIsSelected) {
        control.selectpicker('val', valuesOf(control.find('option')));
      } else {
        control.selectpicker('val', []);
      }
    } else {
      if (allOptionIsSelected && control.val().length != control.find('option').length) {
        control.selectpicker('val', valuesOf(control.find('option:selected[value!="*"]')));
        allOptionIsSelected = false;
      } else if (!allOptionIsSelected && control.val() !== null && control.val().length == control.find('option').length - 1) {
        control.selectpicker('val', valuesOf(control.find('option')));
        allOptionIsSelected = true;
      }
    }
    control.data('allOptionIsSelected', allOptionIsSelected);
  }

});
